if ! echo ${PATH} | grep -q /usr/sue/bin ; then
	PATH=/usr/sue/bin:${PATH}
fi
if ! echo ${PATH} | grep -q /usr/sue/sbin ; then
	if [ `id -u` = 0 ] ; then
		PATH=/usr/sue/sbin:${PATH}
	fi
fi
