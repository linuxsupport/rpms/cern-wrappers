if ( "${path}" !~ */usr/sue/bin* ) then
	set path = ( /usr/sue/bin $path )
endif
if ( "${path}" !~ */usr/sue/sbin* ) then
	if ( `id -u` == 0 ) then
		set path = ( /usr/sue/sbin $path )
	endif
endif
