Summary: CERN wrappers for different utils
Name: cern-wrappers
Version: 1
Release: 18%{?dist}
Vendor: CERN
Packager: Linux.Support@cern.ch
License: GPL
BuildArch: noarch
ExclusiveOS: Linux
Group: System Environment/Base
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Source0: %{name}-%{version}.tgz

Requires: krb5-workstation

%if 0%{?rhel} <= 7
Requires: pam_krb5 >= 2.2.8
%endif

%if 0%{?rhel} >= 8
Requires: sssd-client
%endif

%description
This package supplies wrappers for different
system utilities (klog, kinit, kpasswd .. etc).

These wrapper scripts should only be used at CERN.

%package passwd
Summary: CERN wrappers for different utils
Group: System Environment/Base
Requires: cern-wrappers >= 1-14

%description passwd
This package supplies wrappers for different
system utilities (klog, kinit, kpasswd .. etc).

These wrapper scripts should only be used at CERN.


%prep
%setup -q

%build

%install

mkdir -p $RPM_BUILD_ROOT/usr/sue/bin/
mkdir -p $RPM_BUILD_ROOT/etc/profile.d/

install -m 755 pagsh $RPM_BUILD_ROOT/usr/sue/bin/
#install -m 755 kpasswd $RPM_BUILD_ROOT/usr/sue/bin/
install -m 755 passwd $RPM_BUILD_ROOT/usr/sue/bin/
install -m 755 kinit $RPM_BUILD_ROOT/usr/sue/bin/
#install -m 755 rdesktop $RPM_BUILD_ROOT/usr/sue/bin/
install -m 755 zz_sue.sh $RPM_BUILD_ROOT/etc/profile.d/
install -m 755 zz_sue.csh $RPM_BUILD_ROOT/etc/profile.d/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/etc/profile.d/zz_sue.sh
/etc/profile.d/zz_sue.csh
/usr/sue/bin/pagsh
/usr/sue/bin/kinit

%files passwd
/usr/sue/bin/passwd

%changelog
* Fri Dec 13 2019 Ben Morrice <ben.morrice@cern.ch> 1-18
- add krb5-workstation as a Requires

* Tue Jul 19 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1-17
- update kinit eosfusebind call.

* Thu Mar 03 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1-16
- removed eosfusebind not found warning.

* Tue Mar 01 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1-15
- add eosfusebind to kinit wrapper

* Fri Jan 22 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1-14
- moved passwd wrapper to separate package. (to avoid installation
  in cloud/docker images since it pulls in whole perl
  dependency chain)

* Tue May 13 2014 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1-12
- removed kpasswd wrapper
- changed passwd wrapper text for: AFS -> CERN account

* Tue Feb 11 2014 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1-11
- removed dependency on openafs-krb5

* Fri Mar 9 2012 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1-10.3.slc6
- removed rdesktop wrapper - patched rdesktop 1.6.X.slc6
  fixes the 'licensing error' problem

* Fri Nov 18 2011 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- fix rdesktop wrapper.

* Fri May 27 2011 Jan van Eldik <Jan.van.Eldik@cern.ch> 1-10.1.slc6test
- add rdesktop wrapper to workaround https://bugzilla.redhat.com/show_bug.cgi?id=642554

* Mon Nov 16 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- fix kinit wrapper to exit with error if real kinit did so.

* Tue Aug 19 2008 Jan Iven <jan.iven@cern.ch>
- sync up slc4/slc5 versions

* Tue Jul  1 2008 Jan Iven <jan.iven@cern.ch>
- replace kpasswd wrapper with message

* Wed Jun  4 2008 Jan Iven <jan.iven@cern.ch> 1-6.slc5
- split out for SLC5
   - no more Kerberos4 in "kinit" (MIT branch - just AFS)
   - remove "klog" and "tokens" wrappers - use OpenAFS version in standard path
   - add "pagsh.openafs" to "pagsh" wrapper
   - remove "klist" and "kdestroy" - MIT version should be in user path.

* Fri Feb 22 2008 Jan Iven <jan.iven@cern.ch> 1-6.scl4
- prefer aklog over afs5log since it generates no bogus requests on the server
- prepare to re-route kpasswd to central password page

* Mon Oct  1 2007 Jan Iven <jan.iven@cern.ch> 1.5
- make kinit and klog aware of non-CERN cells/realms (fallback to default behaviour)

* Wed May  2 2007 Jan Iven  <jan.iven@cern.ch> 1.4
- workaround case of missing tickets for kpasswd, IT#115617

* Tue Oct 31 2006 Jan Iven <jan.iven@cern.ch> 1.3
- stronger warning on "klog"
- fix typo in "klist"
- pull in aklog

* Mon Jul 03 2006 Jan Iven <jan.iven@cern.ch> 1.2
- unified version for SLC3/4 again
- pam_krb5-2.2.8 is required (for afs5log)
- slightly stronger warning on the "klog" wrapper

* Tue May 09 2006 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- change preferred kerberos version to MIT

* Wed Jan 25 2006 Jan Iven <jan.iven@cern.ch> 0.9
- fix the totally broken 0.8 release

* Thu Jan 19 2006 Jan Iven <jan.iven@cern.ch> 0.8
- update wrappers to use either Heimdal (preferred) or MIT
- get AFS+Krb4 even with MIT kinit
- remove "Kerberos4 or Kerberos5" logic from kpasswd
* Mon Feb 07 2005 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- removed (confusing) warning from kpasswd wrapper
* Tue Jan 18 2005 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- added info text to kpasswd
* Wed Aug  4 2004 Jan Iven <jan.iven@cern.ch>
- change kpasswd to use the AFS version for now, trigger
  on /afs/cern.ch/project/connectivity/KERBEROS-VERSION for the upgrade
- add passwd (from 7.3 SUE, changed perl path)

* Sun Mar 7 2004 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- initial build
